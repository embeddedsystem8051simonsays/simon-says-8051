//------------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------------
#include <compiler_defs.h>
#include <C8051F020_defs.h>  // SFR declarations
#include <stdlib.h> // rand()
#include <string.h> // pattern tracking for LEDs
//------------------------------------------------------------------------------------
// Global CONSTANTS
//------------------------------------------------------------------------------------
#define SYSCLK 22118400 // Clock frequency of external oscillator on C8051F020 in Hz (22.1184Mhz)
#define SMB_FREQUENCY 80000  // SMBus frequency in Hz (80 Khz)
#define BAUDRATE 115200 // we don't actually need this, but we kinda do

#define  SMB_BUS_ERROR  0x00           // (all modes) BUS ERROR
#define  SMB_START      0x08           // (MT & MR) START transmitted
#define  SMB_RP_START   0x10           // (MT & MR) repeated START
#define  SMB_MTADDACK   0x18           // (MT) Slave address + W transmitted;
                                       //    ACK received
#define  SMB_MTADDNACK  0x20           // (MT) Slave address + W transmitted;
                                       //    NACK received
#define  SMB_MTDBACK    0x28           // (MT) data byte transmitted;
                                       //    ACK rec'vd
#define  SMB_MTDBNACK   0x30           // (MT) data byte transmitted;
                                       //    NACK rec'vd
#define  SMB_MTARBLOST  0x38           // (MT) arbitration lost
#define  SMB_MRADDACK   0x40           // (MR) Slave address + R transmitted;
                                       //    ACK received
#define  SMB_MRADDNACK  0x48           // (MR) Slave address + R transmitted;
                                       //    NACK received
#define  SMB_MRDBACK    0x50           // (MR) data byte rec'vd;
                                       //    ACK transmitted
#define  SMB_MRDBNACK   0x58           // (MR) data byte rec'vd;


#define  YELLOW			0x01 		   // Yellow LED Port #
#define  RED		    0x02 	   	   // Red LED Port #
#define  BLUE			0x04 		   // Blue LED Port #
#define  GREEN			0x08 		   // Green LED Port #
#define  LEDOFF			0x00		   // No LEDs


//-- function prototypes -------------------------------------------------------------
void Init_Clock(void);
void Init_Port(void);
void Init_SMBus(void);
void Init_ADC0(void);
void Init_UART0(void);
void Init_Timer2(unsigned int counts); //Timer 2 is used for the LED pattern control and buzzer.
void Init_Timer3(unsigned int counts);
void Timer3_ISR(void);

// Global variables

// SMBus / I2C bits will be on Port 0 due to
// crossbar configuration in Init_Port()
sbit SDA = P0^2; // Serial data
sbit SCL = P0^3; // Serial clock

unsigned char smb_buf[20];             // SMB transmit/receive buffer
unsigned char smb_len;                 // transfer length
char patternString[40];				   // records pattern for LEDs using Y/R/B/G chars

unsigned int ADC0_reading;
unsigned int cycleCount;			   // used for the LED pattern cycle difficulty
unsigned int fourIterations;		   // game-breaking variable, forcing 4 iterations
unsigned int randomNum;
unsigned int oldRandomNum;
volatile int result;

unsigned int doPatternGeneration;	   // controls whether pattern is generating or not
									   // from timer2 interrupt

void Delay(int k)

{
    int i;
    for (i = 0; i < k; ++i);
}

void SMB_Transmit(unsigned char addr, unsigned char len)
{
    result = 0;

	// First 7 bits are device address
	// last bit indicates we are doing a write
    smb_buf[0] = (0x29 << 1) | 0x00;
    smb_buf[1] = 0x80 | addr; // Command bit of TCS34725 OR'ed with address to transmit to
    smb_len = len;

	// Reset
    STO = 0;

	// Start a transmission
    STA = 1;

    while (result == 0);
    Delay(100);
}

/**
 * @param len - length in bytes of response to receive
 */
void SMB_Receive(unsigned char len)
{
    result = 0;

	// First 7 bits are device address
	// Last bit indicates we are doing a read
    smb_buf[0] = (0x29 << 1) | 0x01;

    smb_len = len;

	// Reset
    STO = 0;

	// Start a recieve
    STA = 1;

    while (result == 0);
    Delay(100);
}

void main(void)
{
	int r, g, b; // these variables will be assigned the RGB values of each sensor reading

	// Disable global interrupts until we have finished
	// initializing.
	EA = 0;

	// Disable watchdog timer
	// Without doing this, the system will reset indefinitely.
	// 0xAD must be sent within 4 system clocks of 0xDE
	WDTCN = 0xDE;
	WDTCN = 0xAD;

	Init_Clock();

    while (!(SDA))
    {
        // Provide clock pulses to allow the slave to advance out
        // of its current state. This will allow it to release SDA.
        XBR2 = 0x40;                     // Enable Crossbar
        P0 &= ~(0x04);                        // Drive the clock low
        Delay(1200);        // Hold the clock low
        P0 |= 0x04;                         // Release the clock
        while(!SCL);                     // Wait for open-drain clock output to rise
        Delay(60);         // Hold the clock high
        XBR2 = 0x00;                     // Disable Crossbar
        Delay(10);
    }

	Init_Port();
	Init_SMBus();
	//Init_ADC0();
	Init_UART0();

	Init_Timer2(384); //Timer 2 is used for the LED pattern control and buzzer.

	// Initialize Timer 3
	// Overflows of Timer 1 will generate interrupts which
	// we can use to initiate conversion of ADC0 input to values we can utilize.
	// Dividing system clock frequecy by crystal frequency in MhZ (12) and ...?
	//Init_Timer3(SYSCLK/12/10);

	// Reset SI flag
	SI = 0;
	// Enable SMBus interrupt
	EIE1 |= 0x02;

	//Init pattern generation to 1 so pattern is created upon start
	doPatternGeneration = 1;
	randomNum = 0;

	// Re-enable global interrupts now that initialization
	// is complete.
	EA = 1;


	SMB_Transmit(0x12, 0); // Get device ID
	SMB_Receive(1);

	if (smb_buf[1] & 0x44) {
		// Update status LED's on 8050 port 0
		// Note that this is an open drain config,
		// we are driving the pin for the LED we want to
		// enable LOW, and the one to disable HIGH.
		P0 |= 0x20; // Unset bad status LED
		P0 &= ~(0x10); // Enable the good status LED

		// Device ID matches TCS34725
		// Power on the device and activates its internal oscillator (1st bit)
		smb_buf[2] = 0x01;
		SMB_Transmit(0x00, 1);
		SMB_Receive(1);

		// We need to wait 2.4ms for the device to be ready
		// Hopefully looping through every value of a
		// 16 bit unsigned number suffices
		Delay(0xFFFF);

		// Activates the device ADC (2nd bit). Make sure to keep the 1st bit set
		smb_buf[2] = 0x01 | 0x02;
		SMB_Transmit(0x00, 1);
		SMB_Receive(1);

		// At this point, we should be able to get readings...
		while(1) {
			// Read the Red value
			// Combine upper and lower bytes into one
			// 16-bit number
			SMB_Transmit(0x16, 2);
			SMB_Receive(2);
			r = (smb_buf[2] << 8) | smb_buf[1];

			// Read the Green value
			// Combine upper and lower bytes into one
			// 16-bit number
			SMB_Transmit(0x18, 2);
			SMB_Receive(2);
			g = (smb_buf[2] << 8) | smb_buf[1];

			// Read the Blue value
			// Combine upper and lower bytes into one
			// 16-bit number
			SMB_Transmit(0x1A, 2);
			SMB_Receive(2);
			b = (smb_buf[2] << 8) | smb_buf[1];

			if(doPatternGeneration == 0) {
				if( (r > g) && (r > b) ) { //Pink (red) Card
					P1 = RED;
				} else if ( (g > r) && (g > b) ) { //Green Card
					P1 = GREEN;
				} else if ( (b > r) && (g > r) ) { //Blue Card
					P1 = BLUE;
				} else if ( (r-g > 0) && (g-b > 0) ) { //Yellow Card
					P1 = YELLOW;
				}
				else {
					P1 = LEDOFF;
				}
			}
		}
	} else {
		// Update status LED's on 8050 port 0
		// Note that this is an open drain config,
		// we are driving the pin for the LED we want to
		// enable LOW, and the one to disable HIGH.
		P0 |= 0x10; // Unset good status LED
		P0 &= ~(0x20); // Enable the bad status LED
	}

	/**
    if (smb_buf[1] & 0x80) {
        unsigned char b = smb_buf[1];
        SMB_Transmit(0, 0);
        smb_buf[2] = b & 0x7F;
        SMB_Transmit(0, 1); // Start DS1307
    }

	// Keep the program running so it can respond
	// to interrupts as they occur
    while(1) {
        SMB_Transmit(0, 0);
        SMB_Receive(14);
        for (i = 0; i < 0x7fff; ++i)
            Delay(0xFFFF);
    }
	**/
}


/*
 * Initialize the system clock
 */
void Init_Clock(void)
{
	// Set external oscillator on board to Crystal Oscillator Mode
	// Set XFCN2-0 to 111 (External Oscillator Frequency Control Bits) since frequency > 6.7Mhz
	OSCXCN = 0x67;

	// Wait for XTLVD pin to be set (highest bit).
	// This indicates that the crystal oscillator has stabilized.
	while ( !(OSCXCN & 0x80) );

	// Disable internal oscillator by setting bit 3 low
	// Enable external oscillator as system clock by setting bit 4 high
	// Enable missing clock detector by setting bit 8 high
	OSCICN = 0x88;
}

/*
 * Configures the Crossbar and GPIO ports
 */
void Init_Port(void)
{
	// Set the first bit of first crossbar register
	// to enable SMBus.
	// Set the fifth bit of first crossbar register
	// to enable UART (but we don't really need it).
	// NOTE: this means port 0.2/0.3 is taken by SMBus SDA/SCL
    XBR0 = 0x05;

	// Unset all bits of second crossbar register (don't
	// care about these peripherals)
	XBR1 = 0x00;

	// Sets the seventh bit of second crossbar register to
	// enable the crossbar.
	XBR2 = 0x40;


	// All ports output mode set to open drain
	// P0.2 and P0.3 are configured as SMBus/I2c in Port_Init(),
	// so for those ports this setting does not have any effect.
	P0MDOUT = 0x00;
	P1MDOUT = 0x0F; // Configure Port 0 for push-pull mode
	P2MDOUT = 0x00;
	P3MDOUT = 0x00;

	// Set port 5 output mode to push pull
	P74OUT = 0x08;

	// Set lower four bits of P5 on (idk why)
	// Set upper four bits off (these control whether LED's are lit)
	P5 |= 0x0F;
	P5 = P5 & 0x0F;
	
	// Set lower four bits of P1 on
	// Set upper four bits off (these control whether the Simon Says LED's are lit)
	P1 |= 0x0F;
	P1 = 0x00;

	P4 = 0xFF;
}

void Init_SMBus(void) {

	// Assert Acknowledge low
	// SMBus free timeout detect
	// SMBus low timeout detect
	SMB0CN = 0x07;

	SMB0CR = 257 - (SYSCLK / (2 * SMB_FREQUENCY));

	// Re-enable SMBus after brief disabling, acts as reset
	SMB0CN |= 0x40;

	STO = 0;
}

// Timer 2 used for LED/Buzzer control. Handles timing (and therefore difficulty)
// Configure Timer2 to auto-reload and generate an interrupt at interval specified
// by <counts> using SYSCLK/12 as it's time base.
void Init_Timer2(unsigned int counts)
{
	CKCON = 0x00; // Define clock (T2M) Timer 2 uses SYSCLK/12
	T2CON = 0x00; // T2 set for timer function (C/T2)
	RCAP2 = counts; // Initialize reload values in the capture register
	T2 = 0xFFFF; // count register set to reload immediately when the first clock occurs
	IE |= 0x20; // Enable timer2 interrupts (ET2)
	T2CON |= 0x04; // Start Timer2 by settings TR2 (T2CON.2) to 1
}

// TODO: Implement timer interrupt for LED pattern, buzzer, and difficulty

void timer_ISR(void) interrupt 5 //Timer 2 interrupt
{	
	T2CON &= ~(0x80); // Clear TF2 by software upon overflow
	if (!doPatternGeneration) {
		return; // Not in this program state, early and efficient return
	}

	if (cycleCount != 16) { // Ridiculous CPU-based timer
		cycleCount++;
		return;
	}
	
	oldRandomNum = randomNum;
	while (oldRandomNum == randomNum)
		randomNum = rand() % 4;
	
	// 0 - YELLOW
	// 1 - BLUE
	// 2 - RED
	// 3 - GREEN
	if (randomNum == 0) {
		P1 = YELLOW;
		strcat(patternString, "Y");
	} else if (randomNum == 1) {
		P1 = BLUE;
		strcat(patternString, "B");
	} else if (randomNum == 2) {
		P1 = RED;
		strcat(patternString, "R");
	} else if (randomNum == 3) {
		P1 = GREEN;
		strcat(patternString, "G");
	}
	
	if (fourIterations == 4) {
		P1 = LEDOFF;
		fourIterations = 0;
		doPatternGeneration = 0;
	}
	else
		fourIterations++;

	cycleCount = 0;
}


void Init_Timer3(unsigned int counts)
{
    TMR3CN = 0x02;
    TMR3RL = -counts;
    TMR3 = 0xffff;
    EIE2 &= ~0x01;
    TMR3CN |= 0x04;
}

void Init_ADC0(void)
{
	// Use internal VREF for ADC0
	// Enable Bias Generator (required for ADC/DAC)
	// Enable Temperature sensor
	REF0CN = 0x07;

	// Set internal amplifier gain to 2
	// Set SAR conversion clock (derived from system clock)
	// AD0SC* is set to 16 by setting the upper bit, resulting in
	// the equation: 16 = (SYSCLOCK / CLK_SAR0) - 1; used to derive the SAR clock rate
	ADC0CF = 0x81;

	// Configure ADC inputs (see register spec for more details)
	AMX0SL = 0x02;

	// Enable ADC
	// Initiate conversion on overflow of timer 3
	// TODO: We might need to change this to timer 2
	// as timer 3 is currently being used by SMBus in which case change from 0x84 to 0x8C
	// It has to be either timer 2 or 3
	ADC0CN = 0x84;
}

void Init_UART0(void) {
    SCON0 = 0x50;
    TMOD = 0x20;       // timer1 as baudrate generator

	#ifdef USE_CKCON
    CKCON |= 0x10;      // timer1 use sysclk
    TH1 = -(SYSCLK/BAUDRATE/16);
	#else
    TH1 = -(SYSCLK/BAUDRATE/16/12);
	#endif

    TR1 = 1;            // start timer1
    PCON |= 0x80;       // SMOD0 = 1
}

/*
 * This routine is called whenever timer 3 overflows.
 */
void Timer3_ISR(void) interrupt 14
{
	SMB0CN &= ~(0x40);
	SMB0CN |= 0x40;

	TMR3CN &= ~(0x80); // clear TF3

	/**
	// Wait for ADC0 conversion to finish
	while ( (ADC0CN & 0x20) == 0);

	// Read ADC0 into an unsigned int
	ADC0_reading = ADC0;

	// Configure bits to enable LED's based on the value
	// of ADC (between 0 and 4096)
	if(ADC0_reading >= 0 && ADC0_reading <= 8) {
		P5 = 0x0F;
	} else if (ADC0_reading > 8 && ADC0_reading < 1024) {
		P5 = 0x1F;
	} else if (ADC0_reading >= 1024 && ADC0_reading < 2048) {
		P5 = 0X3F;
	} else if (ADC0_reading >= 2048 && ADC0_reading < 3072) {
		P5 = 0X7F;
	} else if (ADC0_reading >= 3072 && ADC0_reading <= 4096) {
		P5 = 0XFF;
	}

	ADC0CN &= 0xDF;
	**/
}

void SMBus_ISR(void) interrupt 7
{
    bit FAIL = 0;                     // Used by the ISR to flag failed transfers
    static unsigned char i;             // Used by the ISR to index the buffer

    // Status code for the SMBus (SMB0STA register)
    switch (SMB0STA)
    {
        // Master Transmitter/Receiver: START condition transmitted.
        case SMB_START:
        case SMB_RP_START:
            SMB0DAT = smb_buf[0];          // Slave Address | RW_BIT
            STA = 0;                      // Manually clear the start bit
            i = 0;                        // Reset data byte counter
            break;

            // Master Transmitter: Slave address + WRITE transmitted.  ACK received.
        case SMB_MTADDACK:
            SMB0DAT = smb_buf[1];
            break;

            // Master Transmitter: Slave address + WRITE transmitted.  NACK received.
        case SMB_MTADDNACK:
            FAIL = 1;                  // Indicate failed transfer
            break;

            // Master Transmitter: Data byte transmitted.  ACK received.
            // For a WRITE: each data word should be sent
        case SMB_MTDBACK:
            if (i < smb_len)      // Is there data to send?
            {
                // Send data byte
                SMB0DAT = smb_buf[2 + i];
                i++;
            } else {
                result = 4;
                STO = 1;                 // Set STO to terminte transfer
            }
            break;

            // Master Transmitter: Data byte transmitted.  NACK received.
        case SMB_MTDBNACK:
            FAIL = 1;                  // Indicate failed transfer
            break;

            // Master Transmitter: Arbitration lost.
        case SMB_MTARBLOST:
            FAIL = 1;                     // Indicate failed transfer
            break;

            // Master Receiver: Slave address + READ transmitted.  NACK received.
        case SMB_MRADDNACK:
            FAIL = 1;                  // Indicate failed transfer
            break;

            // Master Receiver: Slave address + READ transmitted.  ACK received.
        case SMB_MRADDACK:
            if (smb_len == 1)
                AA = 0;        // Send NACK
            else
                AA = 1;        // Send ACK
            break;
            // Master Receiver: Data byte received.  ACK transmitted.
        case SMB_MRDBACK:
            if ( i < smb_len)       // Is there any data remaining?
            {
                smb_buf[i + 1] = SMB0DAT;       // Store received byte
                i++;                       // Increment number of bytes received
                AA = 1;                    // Send ACK (may be cleared later
                // in the code)
            }
            if (i >= smb_len)        // This is the last byte
            {
                AA = 0;                    // Send NACK to indicate last byte
            }

            break;

            // Master Receiver: Data byte received.  NACK transmitted.
            // Read operation has completed.  Read data register and send STOP.
        case SMB_MRDBNACK:
            smb_buf[i + 1] = SMB0DAT;      // Store received byte
            STO = 1;
            AA = 1;                       // Set AA for next transfer
            result = 2;
            break;

            // All other status codes invalid.  Reset communication.
        default:
            FAIL = 1;

            break;
    }

    if (FAIL)                           // If the transfer failed,
    {
        SMB0CN &= ~0x40;                 // Reset communication
        SMB0CN |= 0x40;
        STA = 0;
        STO = 0;
        AA = 0;
        result = 1;
        FAIL = 0;
    }

	SI = 0;
}