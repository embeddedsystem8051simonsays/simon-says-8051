# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Simon Says

4 colors for pattern - Red, Green, Yellow, Blue

3 Segment displays will be off initially
LEDs off initially
status LED is red
Press a button to start the game (Same button controls starting and stopping the game)
    * status LED is green
    * 3 segment displays will turn on, displaying “X” to represent available lives
    * LEDs will play the first pattern when the button is pressed (each light will play a separate tone when it lights up)
    * Player holds up cards corresponding to colors up to the RGB sensor. As the cards are recognized, the tone for the
    color provided will play.
    * If player provides wrong card, they will lose a life and the round starts over (error tone maybe?)
    * If player provides correct pattern, next round begins (difficulty increments every 6 rounds)
    * and it goes on like that until the player loses all three of their lives (or turns off the board)

Difficulty;
	The amount of time an LED stays on and time for pattern will decrease as the player plays the game (every 6
        rounds)
         +
        The initial difficulty is controlled by potentiometer setting

        Number of LED’s used in the pattern will increase by one every round


PRESENTATION GUIDELINES
15 minutes
	Overview of project
	Overview of components including technical details and how they work
	Overview of code
	Overview of issues, successes, failures, lessons learned
5 minute
	Demo of Project
Business Casual

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact